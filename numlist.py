#!/usr/bin/env python

# standard numlist
class numlist(object):
	number = None
	digits = None
	base = 10
	list_storage = []

	def __init__(self, number=None, digits=None):
		self.number = number
		self.digits = digits

	def base_space(self, i):
		if type(self.base) is int:
			return self.base
		else:
			return self.base[len(self.base)-i-1]

	def to_list(self):
		num = self.number
		ret = []
		i = 0
		while num > -1:
			ret.append(num % self.base_space(i))
			num /= self.base_space(i)
			if num == 0:
				num -= 1
			i += 1
		ret.reverse()
		return ret

	def pad_list(self, thelist=None, digits=None):
		if thelist is None: thelist = self.list_storage
		if digits is None: digits = self.digits

		return [0]*(digits-len(thelist)) + thelist

	def to_padded_list(self):
		self.list_storage = self.to_list()
		pad_list = self.pad_list()
		return pad_list

# represents a string
class numlist_string(numlist):
	charset = None

	def __init__(self, number, digits, charset):
		super(numlist_string, self).__init__(number, digits)
		self.charset = charset
		self.base = len(charset)

	def __str__(self):
		return self.list_to_chars(self.to_padded_list())

	def list_to_chars(self, thelist):
		ret = ""
		for i in xrange(0, len(thelist)):
			ret += self.charset[thelist[i]]
		return ret

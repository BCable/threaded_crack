#!/usr/bin/env python

import time
from multiprocessing import Process
from multiprocessing.managers import SyncManager

import config

# an indiviual thread for guessing hashes
class guess_thread(Process):
	manager = None
	proc_returns = None
	hash_comp = None
	guesses_iter = None
	algorithm = None
	prefix = None
	postfix = None
	guesses = None

	def __init__(self,
		manager, proc_returns, hash_comp, guesses_iter, algorithm,
		prefix="", postfix=""
	):
		super(guess_thread, self).__init__()
		self.manager = manager
		self.proc_returns = proc_returns
		self.hash_comp = hash_comp
		self.guesses_iter = guesses_iter
		self.algorithm = algorithm
		self.prefix = prefix
		self.postfix = postfix
		self.guesses = manager.Value(int, 0)

	def run(self):
		super(guess_thread, self).run()
		i = 0
		while True:
			try:
				guess = self.guesses_iter.next()
				text = "{}{}{}".format(self.prefix, guess, self.postfix)
				i += 1
				hashed = self.algorithm(text)
				if hashed.digest() == self.hash_comp:
					self.proc_returns.put((i, text))
					break
				elif i % config.PROCESS_UPDATE_COUNT == 0:
					self.guesses.value += config.PROCESS_UPDATE_COUNT
			except StopIteration:
				self.proc_returns.put((i, False))
				break

	def terminate(self):
		self.proc_returns.put((self.guesses.value, False))
		super(guess_thread, self).terminate()

# pass some guesses and start up a threaded guessing mechanism
def run_guesses(
	hash_comp, cracker, algorithm,
	prefix="", postfix="",
	progress=False, debug=False
):
	iter_stopped = False
	threads_running = []
	total_guessed = 0
	total_started = 0

	proc_manager = SyncManager()
	proc_manager.start()
	proc_returns = proc_manager.Queue(config.MAX_THREADS)

	cracker.initialize()

	while True:
		# check return queue, and quit if the hash has completed cracking
		ret = None
		while not proc_returns.empty():
			thread_guessed, answer = proc_returns.get()
			total_guessed += thread_guessed
			if answer is not False:
				if debug: print("WIN:", answer)
				for thread in threads_running:
					thread.terminate()
				ret = answer

		if ret is not None:
			proc_manager.shutdown()
			return (ret, total_guessed)

		# check if any threads terminated
		dead_threads = 0
		for i in xrange(0, len(threads_running)):
			if not threads_running[i-dead_threads].is_alive():
				threads_running.pop(i-dead_threads).terminate()
				if progress: print("GUESSED SO FAR:", total_guessed)
				dead_threads += 1

		# spawn as many new threads as you can
		threads_spawned = 0
		if debug: print(iter_stopped, threads_running, config.MAX_THREADS)
		while not iter_stopped and len(threads_running) < config.MAX_THREADS:
			try:
				guesses = cracker.next()
				total_started += cracker.max_per_thread()

			except StopIteration:
				iter_stopped = True
				break

			p = guess_thread(
				proc_manager, proc_returns,
				hash_comp, guesses, algorithm,
				prefix, postfix
			)
			p.start()
			threads_running.append(p)

			threads_spawned += 1
			del guesses

		if iter_stopped and len(threads_running) == 0 and proc_returns.empty():
			return (False, total_guessed)
		elif threads_spawned == 0:
			time.sleep(config.WAIT_TIME)
			continue

# convert easy to copy/paste and easy to read hexdigest to a regular digest so
# the threads don't have to compute hex and increase string comparison
# computation
def hexdig_tobin(hexdig):
	bindig = ""
	for i in xrange(0, len(hexdig)/2):
		bindig += chr(int(hexdig[i*2:i*2+2], 16))
	return bindig

# wrapper
def crack(generator, algorithm, hash_comp, **kwargs):
	begin_time = time.time()
	answer, total_guessed = run_guesses(hexdig_tobin(hash_comp), generator, algorithm, **kwargs)
	end_time = time.time()
	duration = end_time-begin_time
	return (duration, total_guessed, total_guessed/duration, answer)

ERROR_MARGIN = (config.MAX_THREADS-1)*config.PROCESS_UPDATE_COUNT

#!/usr/bin/env python

LOWER_ALPHA = "".join([chr(i) for i in xrange(97, 97+26)])
UPPER_ALPHA = "".join([chr(i) for i in xrange(65, 65+26)])
DIGITS = "".join([chr(i) for i in xrange(48, 48+10)])
ALL_SYMBOLS = " !@#$%^&*()`~-=_+[]\{}|;':\",./<>?"

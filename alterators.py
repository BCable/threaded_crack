#!/usr/bin/env python

# uppercase one letter
def __upper_case(word, letter):
	return word[0:letter] + word[letter].upper() + word[letter+1:]

# single letter case alterator
def single_case_alterator(word):
	yield word
	for i in xrange(0, len(word)):
		yield __upper_case(word, i)

# all letter case alterator (recursive generator, first time I've made one of those)
def all_case_alterator(word, letter_num=0):
	if len(word) == 1:
		if word.isdigit():
			yield word
		else:
			yield word.lower()
			yield word.upper()
	else:
		recursed_gen = all_case_alterator(word[1:])
		try:
			while True:
				recursed_ret1 = recursed_gen.next()
				recursed_ret2 = recursed_gen.next()
				if word[0].isdigit():
					yield word[0] + recursed_ret1
					yield word[0] + recursed_ret2
				else:
					yield word[0].lower() + recursed_ret1
					yield word[0].upper() + recursed_ret1
					yield word[0].lower() + recursed_ret2
					yield word[0].upper() + recursed_ret2
		except StopIteration:
			pass

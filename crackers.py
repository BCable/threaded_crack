#!/usr/bin/env python

import math

import config, types
from numlist import *

class cracker(object):
	generator = None
	max_guesses_val = None
	max_per_thread_val = None

	def __init__(self, *args):
		super(object)

	def initialize(self):
		self.generator = self.external()

	def next(self):
		return self.generator.next()

	def max_per_thread(self):
		if self.max_per_thread_val is None:
			self.max_per_thread_val = \
				int(math.ceil(self.max_guesses()/config.MAX_THREADS))

		return self.max_per_thread_val

	def max_guesses(self):
		if self.max_guesses_val is not None:
			return self.max_guesses_val

	def internal(self, begin, end):
		return None

	def external(self):
		return None

# simple digit cracker
class digits_cracker(cracker):
	digits = None

	def __init__(self, digits):
		super(digits_cracker, self).__init__()
		self.digits = digits

	def max_guesses(self):
		ret = super(digits_cracker, self).max_guesses()
		if ret is not None: return ret

		self.max_guesses_val = 10**self.digits
		return self.max_guesses_val

	def internal(self, begin, end):
		super(digits_cracker, self).internal(begin, end)
		for i in xrange(begin, end):
			yield str(i).zfill(self.digits)

	def external(self):
		super(digits_cracker, self).external()
		for i in xrange(0, self.max_guesses(), self.max_per_thread()):
			yield self.internal(i, i+self.max_per_thread())

# simple list based character generator
class charset_cracker(cracker):
	charset = None
	digits = None

	def __init__(self, charset, digits):
		super(charset_cracker, self).__init__()
		self.charset = charset
		self.digits = digits

	def max_guesses(self):
		ret = super(charset_cracker, self).max_guesses()
		if ret is not None: return ret

		self.max_guesses_val = len(self.charset)**self.digits
		return self.max_guesses_val

	def internal(self, begin, end):
		super(charset_cracker, self).internal(begin, end)
		for i in xrange(begin, end):
			yield str(numlist_string(i, self.digits, self.charset))

	def external(self):
		for i in xrange(0, self.max_guesses(), self.max_per_thread()):
			yield self.internal(i, i+self.max_per_thread())

# simple dictionary cracker
class dict_cracker(cracker):
	dict_filename = None
	alter_func = None
	lines = None

	def __init__(self, dict_filename, alter_func=None):
		super(dict_cracker, self).__init__()
		self.dict_filename = dict_filename
		self.alter_func = alter_func

	def max_guesses(self):
		ret = super(dict_cracker, self).max_guesses()
		if ret is not None: return ret


		f = open(self.dict_filename, "r")
		self.max_guesses_val = len(f.readlines())
		f.close()
		return self.max_guesses_val

	def internal(self, seek_offset, int_lines):
		f = open(self.dict_filename, "r")
		f.seek(seek_offset)
		for i in xrange(0, int_lines):
			line = f.readline()[0:-1]
			if self.alter_func is None:
				yield line
			else:
				alter_line = self.alter_func(line)

				if type(alter_line) == types.GeneratorType:
					try:
						while True:
							yield alter_line.next()
					except StopIteration:
						pass
				else:
					yield alter_line
		f.close()

	def external(self):
		f = open(self.dict_filename, "r")
		prev_offset = 0
		for i in xrange(0, self.max_guesses(), self.max_per_thread()):
			if i + self.max_per_thread() > self.max_guesses():
				thread_lines = self.max_guesses() - i
			else:
				thread_lines = self.max_per_thread()

			seek_offset = prev_offset
			for j in xrange(0, self.max_per_thread()):
				seek_offset += len(f.readline())

			yield self.internal(prev_offset, thread_lines)
			prev_offset = seek_offset

		f.close()

# complex concatenation cracker
class concat_cracker(cracker):
	crackers = None
	numlist = None

	def __init__(self, *crackers):
		super(concat_cracker, self).__init__()
		self.crackers = crackers

	def max_guesses(self):
		ret = super(concat_cracker, self).max_guesses()
		if ret is not None: return ret

		self.max_guesses_val = self.crackers[0].max_guesses()
		for i in xrange(1, len(self.crackers)):
			self.max_guesses_val *= self.crackers[i].max_guesses()
		return self.max_guesses_val

	def internal(self, begin, end):
		nl = self.numlist
		nl.number = begin
		begin_list = nl.to_padded_list()

		generators = []
		for i in xrange(0, nl.digits):
			generators.append(
				self.crackers[i].internal(
					begin_list[i], self.crackers[i].max_guesses()
				)
			)

		cur_cache = [generators[g].next() for g in range(0, nl.digits)]
		last_list = nl.to_padded_list()
		for i in xrange(begin, end):
			cur_list = nl.to_padded_list()

			if begin != i:
				for i in xrange(nl.digits-1, -1, -1):
					if cur_list[i] == 0:
						generators[i] = self.crackers[i].internal(
							0, self.crackers[i].max_guesses()
						)
					else:
						break

			for i in xrange(0, nl.digits):
				if last_list[i] != cur_list[i]:
					cur_cache[i] = generators[i].next()

			ret = ""
			for i in xrange(0, nl.digits):
				ret += cur_cache[i]

			yield ret

			last_list = cur_list
			nl.number += 1

	def external(self):
		self.numlist = numlist()
		self.numlist.base = [x.max_guesses() for x in self.crackers]
		self.numlist.digits = len(self.crackers)

		for i in xrange(0, self.max_guesses(), self.max_per_thread()):
			yield self.internal(i, i+self.max_per_thread())

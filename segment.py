#!/usr/bin/env python

import sys

class numlist(object):
	charset = None
	number = None
	digits = None
	list_storage = []

	def __init__(self, charset, number, digits):
		self.charset = charset
		self.number = number
		self.digits = digits

	def __str__(self):
		self.list_storage = self.num_to_list()
		padded_list = self.pad_list()
		return self.list_to_chars(padded_list)

	def num_to_list(self):
		num = self.number
		ret = []
		charset_space = len(self.charset)
		while num > -1:
			ret.append(num % charset_space)
			num /= charset_space
			if num == 0:
				num -= 1
		return ret

	def pad_list(self, thelist=None, digits=None):
		if thelist is None: thelist = self.list_storage
		if digits is None: digits = self.digits

		thelist.extend([0]*(digits-len(thelist)))
		return thelist

	def list_to_chars(self, thelist):
		ret = ""
		for i in xrange(0, len(thelist)):
			ret += self.charset[thelist[i]]
		return ret

def charset_generator(charset, digits, begin, end):
	for i in xrange(begin, end):
		yield str(numlist(charset, i, digits))

MAX_NUM = 100

def charset_cracker(charset, digits):
	for i in xrange(0, len(charset)**digits, MAX_NUM):
		yield charset_generator(charset, digits, i, i+MAX_NUM)

lower_alpha = "".join([chr(i) for i in xrange(97, 97+26)])
upper_alpha = "".join([chr(i) for i in xrange(65, 65+26)])
digits = "".join([chr(i) for i in xrange(48, 48+10)])
all_symbols = "!@#$%^&*()`~-=_+[]\{}|;':\",./<>?"

print(lower_alpha, upper_alpha, digits)

number = int(sys.argv[1])
print(numlist(lower_alpha, number, 8))

print(charset_cracker(lower_alpha, 8))

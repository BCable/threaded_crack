#!/usr/bin/env python

import hashlib

import config
from alterators import *
from charsets import *
from crackers import *
from threaded_crack import *

print("ERROR MARGIN: {} GUESSES".format(ERROR_MARGIN))

# hash 0 (solved: "pdf") # TEST
print(10, crack(
	charset_cracker(LOWER_ALPHA, 3),
	hashlib.sha1, "ce9f44bc3d348133b47226685a8f75bbf17e757b"
))

# hash 1 (solved: "1933")
print(1, crack(
	digits_cracker(4),
	hashlib.md5, "1e913e1b06ead0b66e30b6867bf63549"
))

# hash 2 (solved: "1252")
print(2, crack(
	digits_cracker(4),
	hashlib.sha1, "f6d046a7eeb1146f8b1e81aba2dd61fb74655db0"
))

# hash 3 (solved: "4264")
print(3, crack(
	digits_cracker(4),
	hashlib.sha256, "7ddb44478c5fe21a66e7bfe6594e37ad8cc2a5913ec3261a3bda69db8790c92c"
))

# hash 4 (solved: "42511807")
print(4, crack(
	digits_cracker(8),
	hashlib.md5, "177ffcecfcdabe19766d2918899c7b1a"
))

# hash 5 (solved: "16847236")
print(5, crack(
	digits_cracker(8),
	hashlib.sha1, "520cbbf0fba47a0ea4b6e586d2b458844bcb86ed"
))

# hash 6 (solved: "a8014471")
print(6, crack(
	digits_cracker(7),
	hashlib.sha256, "bb7daffb5c8db40f22ec23f703def0b0ba3337593e29f4e8b7bf446c36e99f01",
	prefix="a"
))

# hash 7 (solved: "B7cUbW")
print(7, crack(
	charset_cracker(LOWER_ALPHA + UPPER_ALPHA + DIGITS, 6),
	hashlib.md5, "f1a38aa1764e623510a4a90d5d4aac5a"
))

# hash 8 (solved: ")Ko~'")
print(8, crack(
	charset_cracker(LOWER_ALPHA + UPPER_ALPHA + DIGITS + ALL_SYMBOLS, 5),
	hashlib.sha1, "86690bff465e4319a6f4d26a7773e92c1a2a9cea"
))

# hash 9 (solved: "DhwX[")
print(9, crack(
	charset_cracker(LOWER_ALPHA + UPPER_ALPHA + DIGITS + ALL_SYMBOLS, 6),
	hashlib.md5, "0131f16fc543e2ba9ef624cceecee6b5"
))

# hash 10 (solved: "pdf")
print(10, crack(
	dict_cracker("dicts/aspell.dict", lambda x: x.lower()),
	hashlib.sha1, "ce9f44bc3d348133b47226685a8f75bbf17e757b"
))

# hash 11 (solved: "tobago")
print(11, crack(
	dict_cracker("dicts/hunspell-en_US.dict"),
	hashlib.sha256, "6f4b80a890f15854ea20ebba5d11f3ce6f4237b2f39e49c4831491e4b46036ed"
))

# hash 12 (solved: "chapternonactually")
print(12, crack(
	concat_cracker(
		dict_cracker("dicts/google-10000-english.dict", lambda x: x.lower()),
		dict_cracker("dicts/google-10000-english.dict", lambda x: x.lower()),
		dict_cracker("dicts/google-10000-english.dict", lambda x: x.lower())
	),
	hashlib.md5, "234d1f9461a0a311303ae217810daecb"
))

# hash 13  (solved: "segios")
print(13, crack(
	dict_cracker("dicts/skullsecurity.facebook-firstnames.dict", lambda x: x.lower()),
	hashlib.md5, "ce7776ae88a96ef3e844d5b6d538704c"
))

# hash 14 (solved: "nil99")
print(14, crack(
	concat_cracker(
		dict_cracker("dicts/hunspell-en_US.dict", lambda x: x.lower()),
		digits_cracker(2)
	),
	hashlib.md5, "4c5600213d9f4012cec57e528a534017"
))

# hash 15 (solved: "toulonsurarroux")
print(15, crack(
	dict_cracker("dicts/RockYou.dict"),
	hashlib.sha256, "e9ca088bc1dd085eb1a9c97c9d917d101fd38f6d18ab82f7e443519464c96ace"
))

# hash 16 (solved: "wtvehkji")
print(16, crack(
	charset_cracker(LOWER_ALPHA, 8),
	hashlib.sha1, "3314e5008b5d69bba694586084e14d563e56684a"
))

# hash 17 (solved: "BOHDILCG")
print(17, crack(
	charset_cracker(UPPER_ALPHA, 8),
	hashlib.sha1, "d901f526dfc18fbe404300668ffd4001fd7e315d"
))

### PROBABLY NOT HAPPENING, WILL HAVE TO USE A GPU CRACK OR SOMETHING ###

# hash 18 # TODO
print(18, crack(
	charset_cracker(LOWER_ALPHA + UPPER_ALPHA + DIGITS, 8),
	hashlib.md5, "38b74b814dcd6ba56e199863051731d8"
))

# hash 19 # TODO
print(19, crack(
	charset_cracker(LOWER_ALPHA + UPPER_ALPHA + DIGITS, 10),
	hashlib.md5, "b14b86d1530b6c049925a9f1a10fa8dd"
))
